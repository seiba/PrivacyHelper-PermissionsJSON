# PrivacyHelper-PermissionsJSON
Repository for hosting the app list permissions JSON

## Contents
- Revision key/value: Integer to ensure local (app) JSON copy is up-to-date
- App package name key: A String app package name
- App permission array: A String array of app permissions 

Content is formatted nicely with manual edits and the VS Code plugin JSONPrettify

## Source
The information was obtained over at apptweak.io with my **own** API key

## License
Joshua Lay (c), 2018. All code fragments and information displayed is copyrighted under the MIT license.
